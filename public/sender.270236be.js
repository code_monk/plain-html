//const receiverApplicationId = '956EBFB6';
const receiverApplicationId = chrome.cast.media.DEFAULT_MEDIA_RECEIVER_APP_ID;
const $text = document.getElementById("text");
const $button = document.getElementById("send-text");
let castSession = null;
window['__onGCastApiAvailable'] = (isAvailable)=>{
    if (isAvailable) cast.framework.CastContext.getInstance().setOptions({
        receiverApplicationId,
        autoJoinPolicy: chrome.cast.AutoJoinPolicy.ORIGIN_SCOPED
    });
};
const sendText = ()=>{
    const txt = $text.value;
    castSession = cast.framework.CastContext.getInstance().getCurrentSession();
    if (castSession) castSession.sendMessage('urn:x-cast:ch.cimnine.cromecast-test.text', {
        type: "message",
        text: txt
    });
};
$button.addEventListener('click', sendText);

//# sourceMappingURL=sender.270236be.js.map
